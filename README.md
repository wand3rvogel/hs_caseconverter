# Case Converter
Track: Frontend Core 

While implementing this project, you will work with basic HTML elements. 
Create event handlers for click events and work with strings in JavaScript. 
Find out how to generate files for downloading.

https://hyperskill.org/projects/193
