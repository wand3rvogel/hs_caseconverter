function toTitleCase(str) {
    return str[0].toUpperCase() + str.substring(1).toLowerCase();
}

let textArea = document.querySelector("textarea");

document.getElementById("upper-case")
    .addEventListener("click", () => textArea.value = textArea.value.toUpperCase());

document.getElementById("lower-case")
    .addEventListener("click", () => textArea.value = textArea.value.toLowerCase());

document.getElementById("proper-case")
    .addEventListener("click",
        () => textArea.value = textArea.value.split(" ").map(toTitleCase).join(" "));

document.getElementById("sentence-case")
    .addEventListener("click",
        () => textArea.value = textArea.value.split("\. ").map(toTitleCase).join(". "));

document.getElementById("save-text-file")
    .addEventListener("click", () => download("text.txt", textArea.value));

function download(filename, text) {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

